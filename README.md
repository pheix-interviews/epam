# Remote interview for EPAM

Position details: https://gitlab.com/pheix-interviews/epam/-/blob/main/assets/AM-project-RTOS-Linux.pdf

Quick link: https://gitlab.com/pheix-interviews/epam#feedback-10062023

## Prepare

Previous C experiments: https://replit.com/@pheix

### Initial step

Just write simple `Hello, world!` program with multi threading: https://gitlab.com/pheix-interviews/epam/-/blob/main/src/hello-pthread.c.

### Tasks to verify

1. ✅ [Makefiles](https://spin.atomicobject.com/2016/08/26/makefile-c-projects/): refresh details and create a trivial `Makefile` for compilation and linking;
    * review Makefile specifics like `OBJS := $(addsuffix .o,$(basename $(SRCS)))`: https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html;
2. ✅ C generic things like: memory allocation for dynamic arrays, ~strings~, static arrays, simple algos:
    * reverse array;
    * [sort array](https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC);
    * [binary search in array](https://ru.wikipedia.org/wiki/%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA);
    * functions to inspect:
        * `strcpy`: https://cplusplus.com/reference/cstring/strcpy/
        * `memcpy`: https://cplusplus.com/reference/cstring/memcpy/
        * `strcat`: https://cplusplus.com/reference/cstring/strcat/
3. RTOS related things:
    * ✅ access to shared resource (a few threads and one semaphore);
    * inter-thread communication via message queues;
    * IO related:
        * read/write file;
        * get data from COM port — check `AT gear` application or `Mindwave` project;
    * signal handling (`Monitor library`);
    * trivial HTTP (`RTOS webserver` project).
4. Data structures:
    * ~array length macro;~
    * ✅ lists;
    * trees ([traversals](https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/)):
        * ✅ preorder traversal;
        * inorder traversal;
        * postorder traversal.
5. Go through the PIC sources, just to refresh the principles of SPI and I2C connections (also might be helpful to check RTOS driver documentation);
6. Check and refresh in mind questions from Samsung;

## Feedback 10.06.2023

Summary: **{-&nbsp;application is rejected&nbsp;-}**, final feedback is provided: https://gitlab.com/pheix-interviews/epam/-/blob/main/assets/feedback.pdf.

## Feedback 01.06.2023

Take a look at:

1. [singleton](https://ru.wikipedia.org/wiki/%D0%9E%D0%B4%D0%B8%D0%BD%D0%BE%D1%87%D0%BA%D0%B0_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)): подход к multi-threading программированию;
2. [leetcode](https://leetcode.com/): платформа для совершенстрования навыков программирования;

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
