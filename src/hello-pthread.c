#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void * hello_thread(void * msg);

int main(void) {
	int i;
	pthread_t thid;
	char m[32] = "hello, world!";

	for (i = 0; i < 5; i++) {
		pthread_create(&thid, NULL, hello_thread, &m);
	}

	sleep(1);

	printf("main thread is over\n");

	return 0;
}

void * hello_thread(void * msg) {
	printf("thread 0x%06x: %s\n", (int)pthread_self(), msg);

	return (void *)NULL;
}
