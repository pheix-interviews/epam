#include "../lib/lib.h"

void reverse(int len, int * arr);

int main(void) {
    int * numbers = calloc(ARRAYLEN, sizeof(int));

    init(ARRAYLEN, numbers);

    print(ARRAYLEN, numbers);
    reverse(ARRAYLEN, numbers);
    print(ARRAYLEN, numbers);

    return 0;
}

void reverse(int len, int * arr) {
    for ( int i = 0; i < len / 2; i++ ) {
        int swap = len - 1 - i;
        int curr = arr[i];

        arr[i] = arr[swap];
        arr[swap] = curr;
    }
}
