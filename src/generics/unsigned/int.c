#include "../lib/lib.h"

struct tuple {
    unsigned int value;
};

typedef struct tuple tuple_s;

int main(void) {
    tuple_s s = { .value = 4 };

    while(s.value >= 0) {
        printf("value %02d\n", s.value--);

        sleep(1);
    }

    return 0;
}
