#include "../lib/lib.h"

int f1(void);
int f2(void);

int main(void) {
    /* int (*funtion_ptr)(void) = f1; */
    int value = 0;

    int (*farr[2])(void) = { f1, f2 };

    printf("enter 0 or 1 >");

    scanf("%d", &value);

    if (value != 0 && value != 1) {
        printf("wrong value: should be 0 or 1\n");

        return 1;
    }

    farr[value]();

    return 0;
}

int f1(void) {
    printf("func no.0 is called\n");
}

int f2(void) {
    printf("func no.1 is called\n");
}
