#include "lib.h"

void init(int len, int * arr) {
    srand(time(NULL));

    for ( int i = 0; i < len; i++ ) {
        arr[i] = rand() * 0.000001;
    }
}

void print(int len, int * arr) {
    for ( int i = 0; i < len; i++ ) {
        printf("%02d: %d\n", i, arr[i]);
    }
}

void bubble_sort(int len, int * arr) {
    int sort = 1;

    while(sort) {
        sort = 0;

        for(int i = 0; i < len - 1; i++ ) {
            if (arr[i] > arr[i + 1]) {
                int swap = arr[i];

                arr[i] = arr[i + 1];
                arr[i + 1] = swap;

                sort = 1;
            }
        }
    }
}
