#ifndef __LIBH__
#define __LIBH__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define ARRAYLEN 16

extern void init(int len, int * arr);
extern void print(int len, int * arr);
extern void bubble_sort(int len, int * arr);

#endif /* __LIBH__ */
