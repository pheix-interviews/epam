#include "../lib/lib.h"

int main(void) {
    int * numbers = calloc(ARRAYLEN, sizeof(int));

    init(ARRAYLEN, numbers);

    bubble_sort(ARRAYLEN, numbers);
    print(ARRAYLEN, numbers);

    return 0;
}
