#include "../lib/lib.h"

void binary_search(int value, int start_index, int end_index, int * arr);

int main(void) {
    int search_value = 0;
    int * numbers = calloc(ARRAYLEN, sizeof(int));

    init(ARRAYLEN, numbers);
    bubble_sort(ARRAYLEN, numbers);
    print(ARRAYLEN, numbers);

    printf("enter value to search> ");

    scanf("%d", &search_value);

    binary_search(search_value, 0, ARRAYLEN - 1, numbers);

    return 0;
}

void binary_search(int value, int start_index, int end_index, int * arr) {
    static int iter = 1;

    int len   = end_index - start_index;
    int index = start_index + len / 2;

    printf("trying %d (%d)\n", index, len);

    if (arr[index] == value) {
        printf("found %d by %d iterations\n", value, iter);

        return;
    }

    if (len <= 0) {
        return;
    }

    iter++;

    if (arr[index] > value) {
        binary_search(value, start_index, index, arr);
    }
    else {
        binary_search(value, index + 1, end_index, arr);
    }
}
