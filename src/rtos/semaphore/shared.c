#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define WORKERS 10

int static resource = 0;
sem_t semaphore;

void * thread(void * arg);

int main(void) {
    int args[WORKERS];
    pthread_t tid[WORKERS];
    pthread_attr_t attrs;

    sem_init(&semaphore, 0, 1);

    pthread_attr_init(&attrs);

    for (int i = 0; i < WORKERS; i++ ) {
        args[i] = i + 1;

        pthread_create(&tid[i], &attrs, thread, &args[i]);
    }

    for (int i = 0; i < WORKERS; i++ ) {
        pthread_join(tid[i], NULL);

        //printf("thread 0x%06x finished\n", (unsigned int)tid[0]);
    }

    printf("exiting...\n");

    sem_close(&semaphore);

    return 0;
}

void * thread(void * arg) {
    int index = *(int *)arg;

    sem_wait(&semaphore);

    printf("thread 0x%06x started with wait index %d (resource=%d)\n", (int)pthread_self(), index, resource);

    /* doing smth with resourse */
    resource++;

    sleep(index);

    sem_post(&semaphore);

    return (void*)NULL;
}
