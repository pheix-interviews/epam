#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct treeNode_s {
    int data;
    struct treeNode_s * left;
    struct treeNode_s * right;
} treeNode;

void traverse(treeNode * tree);
treeNode * create_node(int data);
void push(treeNode ** tree, int data);

int main(void) {
    treeNode * tree = NULL;

    int nums[10] = {7,3,6,2,5,10,8,4,9,1};

    for (int i = 0; i < 10; i++ ) {
        push(&tree, nums[i]);
    }

    printf("tree is ready!\n");

    traverse(tree);

    return 0;
}

void traverse(treeNode * tree) {
    if (tree == NULL) {
        return;
    }

    printf("node %02d\n", tree->data);

    if (tree->left != NULL) {
        traverse(tree->left);
    }

    if (tree->right != NULL) {
        traverse(tree->right);
    }
}

treeNode * create_node(int data) {
    treeNode * node = calloc(1, sizeof(treeNode));

    node->data  = data;
    node->left  = NULL;
    node->right = NULL;

    return node;
}

void push(treeNode ** tree, int data) {
    treeNode * node = create_node(data);

    treeNode * localtree = *tree;

    if (localtree == NULL)  {
        *tree = node;
    }
    else if (localtree->data < data) {
        if (localtree->left == NULL) {
            localtree->left = node;
        }
        else {
            push(&localtree->left, data);
        }
    }
    else if (localtree->data > data) {
        if (localtree->right == NULL) {
            localtree->right = node;
        }
        else {
            push(&localtree->right, data);
        }
    }
}
