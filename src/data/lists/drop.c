#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define LENGTH 10

struct Node {
    int    value;
    void * prev;
    void * next;
};

typedef struct Node Node;

void traverse(Node * node);
Node * create_list(int len, Node * root);
Node * delete_elem_by_value(int value, Node * node);

int main(void) {
    Node * root;
    int delete_value = 0;

    root = create_list(LENGTH, root);
    traverse(root);

    printf("value to delete> ");

    scanf("%d", &delete_value);

    root = delete_elem_by_value(delete_value, root);
    traverse(root);

    return 0;
}

Node * create_list(int len, Node * node) {
    srand(time(NULL));

    Node * root      = NULL;
    Node * prev_node = NULL;

    for ( int i = 0; i < len; i++ ) {
        Node * new_node = calloc(1, sizeof(Node));

        new_node->value = rand() * 0.0000001;
        new_node->next  = NULL;

        if (prev_node != NULL) {
            prev_node->next = new_node;
            new_node->prev  = prev_node;
        }
        else {
            new_node->prev = NULL;
            root = new_node;
        }

        printf("node %02d = %d\n", i, node->value);

        prev_node = new_node;
    }

    return root;
}

void traverse(Node * node) {
    int i = 0;

    Node * curr_node = node;

    while(1) {
        printf("iterating node %02d with value %4d\n", i, curr_node->value);

        if (curr_node->next == NULL) {
            break;
        }
        else {
            curr_node = curr_node->next;
        }

        i++;
    }

    return (void)NULL;
}

Node * delete_elem_by_value(int value, Node * node) {
    Node * curr_node = node;

    while(1) {
        if (curr_node->value == value) {
            Node * prev_node = curr_node->prev;

            if (prev_node == NULL) {
                printf("remove node with value %04d\n", curr_node->value);

                Node * new_root = curr_node->next;

                free(curr_node);

                return new_root;
            }

            if (curr_node->next) {
                prev_node->next = curr_node->next;
            }
            else {
                prev_node->next = NULL;
            }

            printf("remove node with value %04d\n", curr_node->value);

            free(curr_node);

            return node;
        }

        if (curr_node->next == NULL) {
            return node;
        }
        else {
            curr_node = curr_node->next;
        }
    }
}
