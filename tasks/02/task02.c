#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct node_s {
    int data;
    struct node_s * next;
} Node;

Node * pop(Node * head, int* data);
Node * push(Node * node, int data);

int main (void) {
    Node * head = NULL;

    head = push(head, 1);
    head = push(head, 2);
    head = push(head, 3);

    // head 3 -> 2 -> 1

    // head 3 <- 2 <- 1 tail

    /* Тема в том, что нужен порядок такой же как был на входе - FIFO, а у нас LIFO*/

    int data;
    head = pop(head, &data);

    // 3< head 2 -> 1
    printf("%d\n", data);

    head = pop(head, &data);

    // 2< head 1
    printf("%d\n", data);

    head = pop(head, &data);

    // 1< head null
    printf("%d\n", data);

    return 0;
}

Node * pop(Node * head, int* data) {
    if (head == NULL) {
        return NULL;
    }

    *data = head->data;

    Node * new_head = head->next;

    free(head);

    return new_head;
}

Node * push(Node * node, int data) {
    Node * new_elem = calloc(1, sizeof(Node));

    new_elem->data = data;
    new_elem->next = node;

    if (node == NULL) {
        return new_elem;
    }

    return new_elem;
}
