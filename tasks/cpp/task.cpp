#include <iostream>
#include <string>

struct Test
{
    std::string str = "str\n";
    void f()
    {
        std::cout << "hello\n";
    }
    void g()
    {
        std::cout << str;
    }
};

int main ()
{
    Test* obj = (Test*) nullptr;
    obj->f();
    obj->g();
}
