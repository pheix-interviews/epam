/*
{
   char res[100];
   strcpy(res, a1);
   strcat(res, a2);
   return res;
}

Look at res[100], it's allocated on stack and works within scope { },
so it's not available outside the function and return statement will return
garbage/blank value (pointer to). To return the pointer to actual string, we
have to explicitly allocate memory via calloc/malloc.

Check out allocation basics: https://pawn.wiki/index.php?/topic/42005-stak-segment-dannii-kucha/

*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

const char * f1(const char *a1, const char *a2);

const char * f1(const char *a1, const char *a2)
{
   char * res = calloc(strlen(a1) + strlen(a2) + 1, sizeof(char));
   strcpy(res, a1);
   strcat(res, a2);

   return res;
}


int main(void) {
    const char * r = f1("a1", "a2");

    printf("%s\n", r);

    free((void *)r);
}
